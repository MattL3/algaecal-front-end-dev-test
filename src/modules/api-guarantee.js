export default function loadGuarantee(id = 'api-guarantee-target') {
      const ajax = new XMLHttpRequest();
      ajax.open('GET', 'https://www.algaecal.com/wp-json/acf/v3/options/options/');
      ajax.send();
      ajax.onload = () => {
        const div = document.getElementById(id);
        const parsed = JSON.parse(ajax.response);
   
        div.innerHTML = parsed.acf['7yr_full_copy'];
      };
  }
  