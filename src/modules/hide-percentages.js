export default function hidePercentages(classname = 'percent-off') {
    Array.from(document.getElementsByClassName(classname)).forEach((el) => {
        if(el.getAttribute('data-base-savings-amount') == 0) {
            el.classList.add('hidden')
        }
    });
}