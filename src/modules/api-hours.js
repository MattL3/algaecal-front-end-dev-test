export default function loadHours(id = 'api-hours-target') {
  const ajax = new XMLHttpRequest();
  ajax.open('GET', 'https://www.algaecal.com/wp-json/acf/v3/options/options/');
  ajax.send();
  ajax.onload = () => {
    const div = document.getElementById(id);
    const parsed = JSON.parse(ajax.response);

    //Get current date/time of user 
    let d = new Date();
    //Increment value to align with office_hours key "Day" index. 
    const dateConversion = (d.getDay() + 1);

    //loop through days checking for day which matches current
    for (let i = 0; i < parsed.acf['office_hours'].length; i++ ) {
        if (parsed.acf['office_hours'][i]['day'] == dateConversion) {
            
        //Sanitize User date/time to Vancouver time for comparison
        const offset = -7;
        let d = new Date();

        let utc = d.getTime() + (d.getTimezoneOffset() * 60000);

        let nd = new Date(utc + (3600000*offset));

        const dlocale = nd.toLocaleTimeString();
        
        //Subtract Seconds and colons from 24hour time
        
        let dv = dlocale.substring(0, dlocale.length - 3);
        dv = dv.replace(":", "");

        //if User time is greater than starting time and less than closing time remove hidden class attribute from call center divs
        if (dv > parsed.acf['office_hours'][i]['starting_time'] && dv < parsed.acf['office_hours'][i]['closing_time']) {
            div.classList.remove("hidden");
        }
      } else {
        continue;
      }
    };
  }
}