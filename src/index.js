// import {ProductBundles} from "./modules/ProductBundles";
import './styles/main.scss';
import loadSVGs from './modules/svg-replace';
import hidePercentages from './modules/hide-percentages';
import loadGuarantee from './modules/api-guarantee';
import loadHours from './modules/api-hours';
import 'popper.js';
import 'bootstrap';

document.addEventListener('DOMContentLoaded', () => {
  loadSVGs();
  hidePercentages();
  loadGuarantee();
  loadHours();
});
